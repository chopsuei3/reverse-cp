import csv
import math



def reverse_cp(cp_search):
		valid_pokemon = {}
		pokemon_list = []
		options_dict = {}

		with open("dex.csv") as f:
			reader = csv.reader(f)
			next(reader) # skip header
		#	data = []
			for row in reader:
				 pokedex = int(row[0])
				 pokemon = row[1]
				 hp = float(row[2])
				 attack = float(row[3])
				 defense = float(row[4])
				 max_cp = int(row[5])
				 iv_dict = {}

				 if(cp_search > max_cp):
					 continue

				 for i in range(2,81):
					 level = i/2.0
					 data = []


					 for atk_modifier in range(0,16):
						 for def_modifier in range(0,16):
							 for hp_modifier in range(0,16):
		#						 print atk_modifier, def_modifier, hp_modifier
								 atk_total = attack + atk_modifier
								 def_total = defense + def_modifier
								 hp_total = hp + hp_modifier
								 iv = round(((atk_modifier + def_modifier + hp_modifier)/45.0)*100,2)
		#						 iv_dict[iv] = data
		#						 iv_tuple = (level, iv)

								 if (level <= 10):
									 f_level = (0.01885225*level)-0.01001625
								 elif (level <= 20):
									 f_level = (0.01783805*(level-10))+0.17850625
								 elif (level <= 30):
									 f_level = (0.01784981*(level-20))+0.35688675
								 elif (level <= 40):
									 f_level = (0.00891892*(level-30))+0.53538485
		#						 cp = math.floor(atk_total * math.sqrt(def_total) * math.sqrt(hp_total) * (f_level / 10))
								 cp = math.trunc(math.floor(atk_total * math.sqrt(def_total) * math.sqrt(hp_total) * (f_level / 10)))
								 if cp == cp_search:
		#						 	 print pokemon, level, cp, iv, atk_modifier, def_modifier, hp_modifier
		#							 print level, cp
		#							 print str(level) + " " + str(cp),
		#							 print "*Match* Level " + str(level) + " " + str(pokemon) + " at " + str(iv) + "%"
		#							 print pokemon, level, cp, iv, atk_modifier, def_modifier, hp_modifier
									 if pokemon not in pokemon_list:
										 pokemon_list.append(pokemon)
		#							 if level not in data:
									 if level not in data and iv > 90:
										 data.append("Level " + str(level) + " " + str(atk_modifier) + " " + str(def_modifier) + " " + str(hp_modifier))
		#								 data.append(level)
										 iv_dict[iv] = data
										 valid_pokemon[pokemon] = iv_dict
			   
		#for cp, options in options_dict.iteritems():
		#	print cp, options
		for pokemon, combos in valid_pokemon.iteritems():
			print pokemon #, combos
			for k, v in sorted(combos.items()):
				print k, ':', v

